﻿using GalaSoft.MvvmLight.Messaging;
using GenericStore.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GenericStore
{
    /// <summary>
    /// Interaction logic for PreparationsWindow.xaml
    /// </summary>
    public partial class PreparationsWindow : Window
    {

        public PreparationsWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();


            Messenger.Default.Register<NotificationMessage>(this, (message) =>
            {
                switch (message.Notification)
                {
                    case "LogoutWindow":
                        Messenger.Default.Send(new NotificationMessage("NewCourse"));
                        var loginwindow = new Login();
                        loginwindow.Show();
                        this.Close();
                        Messenger.Default.Unregister(this);
                        break;
                }

            });


        }
    }
}
