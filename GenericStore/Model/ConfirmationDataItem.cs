﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GenericStore.Model
{
    public class ConfirmationDataItem
    {
        public class data_collection : ViewModelBase
        {

            public string or { get; set; }
            public string verification { get; set; }
            public string address { get; set; }
            public string additional_info { get; set; }
            public double longitude { get; set; }
            public double latitude { get; set; }
            public double store_longitude { get; set; }
            public double store_latitude { get; set; }
            public string customer_contact { get; set; }

            public DateTime date { get; set; }
            private GroupBox _group;
            public GroupBox group
            {
                get { return _group; }


                set
                {
                    if (_group != value)
                    {
                        _group = value;
                        RaisePropertyChanged("group");

                    }
                }
            }
        }

        public class order_data
        {
            public string or { get; set; }
            public string customer_name { get; set; }
            public string customer_location { get; set; }
            public double customer_longitude { get; set; }
            public double customer_latitude { get; set; }
            public double store_longitude { get; set; }
            public double store_latitude { get; set; }
            public string customer_contact { get; set; }
            public string customer_cash { get; set; }
            public DateTime date { get; set; }
            public string verification { get; set; }
            public string additional_info { get; set; }
            public string order_status { get; set; }
            public List<ordered_items> orders { get; set; }
        }

        public class ordered_items
        {
            public string order_category { get; set; }
            public string order_name { get; set; }
            public string order_size { get; set; }
            public string order_type { get; set; }
            public int order_quantity { get; set; }
            public float order_price { get; set; }

        }
    }
}
