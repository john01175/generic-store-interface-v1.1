﻿using System.Windows;
using GenericStore.ViewModel;
using GalaSoft.MvvmLight.Messaging;

namespace GenericStore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ConfirmationWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public ConfirmationWindow()
        {
            InitializeComponent();
         //   Closing += (s, e) => ViewModelLocator.Cleanup();

            Messenger.Default.Register<NotificationMessage>(this, (message) =>
            {
                switch (message.Notification)
                {
                    case "LogoutWindow":
                        Messenger.Default.Send(new NotificationMessage("NewCourse"));
                        var loginwindow = new Login();
                        loginwindow.Show();
                        this.Close();
                        break;
                }

            });
        }
    }
}