﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericStore.Model
{
    public class LoginDataItem
    {
        public class user
        {
            public string username { get; set; }
            public string password { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string role { get; set; }
            public string store_id { get; set; }
        }
    }
}
