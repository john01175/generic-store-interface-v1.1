﻿using GalaSoft.MvvmLight.Messaging;
using GenericStore.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GenericStore
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();


            //nag hihintay lang ng message galing viewmodel ng login..
            Messenger.Default.Register<NotificationMessage>(this, (message) =>
            {
                switch (message.Notification)
                {
                    case "OpenConfirmation":
                        Messenger.Default.Send(new NotificationMessage("NewCourse"));
                        var confirmation = new ConfirmationWindow();
                        confirmation.Show();
                        Messenger.Default.Unregister(this);
                        this.Close();
                        break;

                    case "OpenPreparation":
                        Messenger.Default.Send(new NotificationMessage("NewCourse"));
                        var preparation = new PreparationsWindow();
                        preparation.Show();
                        Messenger.Default.Unregister(this);
                        this.Close();
                        break;
                }

            });
        }
    }
}
