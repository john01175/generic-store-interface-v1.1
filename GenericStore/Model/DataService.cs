﻿using RestSharp;
using System;
using System.Collections.Generic;
using static GenericStore.Model.LoginDataItem;
using System.Collections.ObjectModel;
using System.Collections;

namespace GenericStore.Model
{
    public class DataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to connect to the actual data service

            var item = new DataItem("Welcome to MVVM Light");
            callback(item, null);
        }

        public List<user> GetCredentials(string username, string password)
        {
            RestClient client = new RestClient("http://chatbotservices.somee.com");
            var getRequest = new RestRequest("bot_store/api/Login?username=" + username + "&password=" + password, Method.GET);
            var getResult = client.Execute<List<user>>(getRequest).Data;
            return getResult; 
        }
    }
}