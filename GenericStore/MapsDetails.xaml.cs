﻿using GMap.NET;
using GMap.NET.MapProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using GMap.NET.WindowsPresentation;
using System.Windows.Threading;

namespace GenericStore
{
    /// <summary>
    /// Interaction logic for MapsDetails.xaml
    /// </summary>
    ///
    public partial class MapsDetails : Window
    {
        private double _longitude;
        private double _latitude;
        private double _store_longitude;
        private double _store_latitude;
        PointLatLng start;
        PointLatLng end;
      

        public MapsDetails()
        {
            InitializeComponent();
        }

        public void custDetails(double longitude, double latitude, string address, string additionalInfo, double store_longitude, double store_latitude)
        {
            lblPhysicalAddress.Content = address;
            lblNotes.Content = additionalInfo;
            _longitude = longitude;
            _latitude = latitude;
            _store_latitude = store_latitude;
            _store_longitude = store_longitude;

        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            gMap.MapProvider = GMapProviders.OpenStreetMap;
            GMaps.Instance.Mode = AccessMode.ServerAndCache;
            gMap.ShowCenter = false;      
     
        }

        private void gMap_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        public void AddMarker(double longitude, double latitude, double store_longitude, double store_latitude)
        {
            
            PointLatLng destination_point = new PointLatLng(longitude, latitude);
            this.end = destination_point;
            gMap.Position = new PointLatLng(longitude, latitude);
            GMapMarker destination_marker = new GMapMarker(destination_point);
            destination_marker.Shape = new Ellipse
            {
                Width = 20,
                Height = 20,
                Fill = Brushes.Teal,
                Stroke = Brushes.Black,
                ToolTip = "Destination",
                StrokeThickness = 1.5
            };

            gMap.Markers.Add(destination_marker);
       

            PointLatLng store_point = new PointLatLng(store_latitude, store_longitude);
            this.start = store_point;
            GMapMarker store_marker = new GMapMarker(store_point);
            store_marker.Shape = new Ellipse
            {
                Width = 20,
                Height = 20,
                Fill = Brushes.Red,
                Stroke = Brushes.Gray,
                ToolTip = "Store",
                StrokeThickness = 1.5
            };

            gMap.Markers.Add(store_marker);
            
        }

        private void gMap_OnTileLoadComplete(long ElapsedMilliseconds)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                   new Action(() =>

                 AddMarker(_longitude, _latitude, _store_longitude, _store_latitude)
                 //gMap.Markers.Add(Mar);
            ));

        }

        private void btnRoute_Click(object sender, RoutedEventArgs e)
        {
         
            RoutingProvider rp = gMap.MapProvider as RoutingProvider;
            if (rp == null)
            {
                rp = GMapProviders.OpenStreetMap; // use OpenStreetMap if provider does not implement routing
            }

            MapRoute route = rp.GetRoute(start, end, false, false, 12);

           
            if (route != null)
            {
                GMapRoute mRoute = new GMapRoute(route.Points);
                {
                    mRoute.ZIndex = -1;
                }

                gMap.Markers.Add(mRoute);

                gMap.ZoomAndCenterMarkers(null);
            }
            else
            {
               MessageBox.Show("There is no route");
            }

        }
    }
}
