﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using static GenericStore.Model.LoginDataItem;

namespace GenericStore.Model
{
    public interface IDataService
    {
        void GetData(Action<DataItem, Exception> callback);
        List<user> GetCredentials(string username, string password);
    }
}
