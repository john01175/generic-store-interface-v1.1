﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GenericStore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static GenericStore.Model.LoginDataItem;

namespace GenericStore.ViewModel
{
    public class LoginViewModel: ViewModelBase
    {
        public IDataService _dataService;
        public RelayCommand<object> LoginCommand { get; private set; }
        public RelayCommand ExitCommand { get;  private set;}

        public LoginViewModel(IDataService dataservice)
        {
            _dataService = dataservice;
            LoginCommand = new RelayCommand<object>((obj) => Login(obj), CanLogin);
            ExitCommand = new RelayCommand(Exit);
        }

        private void Exit()
        {
            Environment.Exit(0);
        }

        private bool CanLogin(object arg)
        {
            return !string.IsNullOrEmpty(Username);
        }
        
        private void Login(object obj)
        {
            //needs password encryption... to follow nalang..
            Mouse.OverrideCursor = Cursors.Wait;
            var passwordBox = obj as PasswordBox;
            var password = passwordBox.Password;

          
            var result = _dataService.GetCredentials(Username, password.ToString());

            if (result != null)
            {
                string role = "";
                List<user> dataList = new List<user>();
                foreach (var data in result)
                {
                    dataList.Add(new user()
                    {
                        firstname = data.firstname,
                        lastname = data.lastname,
                        role = data.role,
                        store_id = data.store_id

                    });

                    role = data.role;
                }

                if (role.ToLower() == "cashier")
                {
                    Messenger.Default.Send(new NotificationMessage("OpenConfirmation"));

                }

                else if (role.ToLower() == "kitchen")
                {
                    Messenger.Default.Send(new NotificationMessage("OpenPreparation"));
                }

                Messenger.Default.Send<List<user>>(dataList, "Message");
                Mouse.OverrideCursor = Cursors.Arrow;
                Username = string.Empty;
                passwordBox.Password = string.Empty;

            }

            else
            {
                Mouse.OverrideCursor = Cursors.Arrow;
                MessageBox.Show("Incorrect username or password");
                Username = string.Empty;
                passwordBox.Password = string.Empty;
            }

           
        }

        private string _username = string.Empty;
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                RaisePropertyChanged("Username");
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

 
    }


}
