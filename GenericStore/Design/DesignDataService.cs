﻿using System;
using System.Collections.Generic;
using GenericStore.Model;
using RestSharp;
using static GenericStore.Model.LoginDataItem;

namespace GenericStore.Design
{
    public class DesignDataService : IDataService
    {
        public List<user> GetCredentials(string username, string password)
        {
            RestClient client = new RestClient("http://chatbotservices.somee.com");
            var getRequest = new RestRequest("bot_store/api/Login?username=" + username + "&password=" + password, Method.GET);
            var getResult = client.Execute<List<user>>(getRequest).Data;
            return getResult;
        }

        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }
    }
}