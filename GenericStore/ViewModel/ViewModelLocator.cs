﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:GenericStore.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using GenericStore.Model;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;

namespace GenericStore.ViewModel
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
           //registation of viewmodels and dataservice
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
              // SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<StatusViewModel>();
            SimpleIoc.Default.Register<ConfirmationsViewModel>();
            SimpleIoc.Default.Register<PreparationsViewModel>();

        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }


        public LoginViewModel Login
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LoginViewModel>();
            }
        }

        public StatusViewModel Stats
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StatusViewModel>();
            }
        }

        public ConfirmationsViewModel Confirmations
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ConfirmationsViewModel>();
            }
        }

        public PreparationsViewModel Preparations
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PreparationsViewModel>();
            }
        }


        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
           // Messenger.Default.Unregister(this);

        }
    }
}