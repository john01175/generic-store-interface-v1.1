﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using static GenericStore.Model.ConfirmationDataItem;
using static GenericStore.Model.LoginDataItem;

namespace GenericStore.ViewModel
{
    public class ConfirmationsViewModel: ViewModelBase
    {
        public string StoreID;
        public string p;


        SerialPort port = new SerialPort();
        clsSMS objclsSMS = new clsSMS();
        ShortMessageCollection objShortMessageCollection = new ShortMessageCollection();

        //timers
        DispatcherTimer durationTimer = new DispatcherTimer();
        DispatcherTimer queryTimer = new DispatcherTimer();
        DateTime currentdt;

        //collection of data
        public ObservableCollection<data_collection> dynamicConfirm { get; set; } = new ObservableCollection<data_collection>();
        ObservableCollection<GroupBox> groupings = new ObservableCollection<GroupBox>();

        public List<string> comport { get; set; } = new List<string>();
        List<order_data> tempoList;


        public RelayCommand ConnectCommand { get; private set; }


        //styles
        Style greenGroupbox = (Style)Application.Current.Resources["Green"];
        Style yellowGroupbox = (Style)Application.Current.Resources["Yellow"];
        Style redGroupbox = (Style)Application.Current.Resources["Red"];
        Style deliver_button_style = (Style)Application.Current.Resources["deliver_button_style"];
        Style address_button_style = (Style)Application.Current.Resources["address_button_style"];

        RestClient client = new RestClient("http://chatbotservices.somee.com");

        public ConfirmationsViewModel()
        {
            //get data sent from login
            Messenger.Default.Register<List<user>>(this, "Message", (dataList) =>
            {
                foreach (var data in dataList)
                {

                    pending(data.store_id);

                }
            });

           
            d_timer();
            query_timer();

            #region Display all available COM Ports
            string[] ports = SerialPort.GetPortNames();

            // Add all port names to the combo box:
            foreach (string port in ports)
            {
                comport.Add(port);
            }
            #endregion

            ConnectCommand = new RelayCommand(ConnectGSM);

        }

        private void ConnectGSM()
        {
            this.port = objclsSMS.OpenPort(this.Comport, 9600, 8, 300, 300);

            if (this.port != null)
            {
                MessageBox.Show("Connected");
            }

            else
            {
                MessageBox.Show("Invalid Comport Settings");
            }
        }

        #region Duration Timer Initialization
        private void d_timer()
        {
            durationTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            durationTimer.Interval = new TimeSpan(0, 0, 1);
            durationTimer.Start();
        }
        #endregion

        #region Duration Timer
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            //txtNofConfirmation = dynamicConfirm.Count.ToString();
            this.currentdt = DateTime.Now;

            if (groupings != null)
            {
                for (int i = 0; i < groupings.Count; i++)
                {
                    TimeSpan span = this.currentdt - dynamicConfirm[i].date;
                    double difference = span.TotalSeconds;
                    TimeSpan sec = TimeSpan.FromSeconds(difference + 30);
                    double hours = 0;
                    double minutes = 0;

                    if (difference > 60)
                    {
                        hours = (difference / 60) / 60;
                        minutes = (((hours * 100) % 100) / 100) * 60;
                    }

                    else
                    {
                        minutes = difference / 60;

                    }

                    if ((minutes <= 5) && (hours < 1))
                    {
                        groupings[i].Style = greenGroupbox;
                    }

                    else if ((minutes > 5) && (minutes < 10) && (hours < 1))
                    {
                        groupings[i].Style = yellowGroupbox;
                    }

                    else
                    {
                        groupings[i].Style = redGroupbox;
                    }

                    groupings[i].Header = "Duration: " + hours.ToString("0") + "h: " + minutes.ToString("00") + "m: " + sec.Seconds.ToString("00") + "s";
                }
            }
        }
        #endregion

        #region Query Timer Initialization
        private void query_timer()
        {
            queryTimer.Tick += new EventHandler(queryTimer_Tick);
            queryTimer.Interval = new TimeSpan(0, 0, 1);
            queryTimer.Start();
        }
        #endregion

        #region Query Timer
        private async void queryTimer_Tick(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                string pendings = dynamicConfirm.Count.ToString();
                Messenger.Default.Send(pendings, "Pendings");
                   

                var getRequest = new RestRequest("bot_store/api/store", Method.GET);
                string getResult = client.Execute(getRequest).Content;
                getResult = getResult.Trim('"');

                //if the new data is equal to the logged in store id
                if ((getResult != "none") && (getResult == StoreID))
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        //get the data based on store id
                        new_order_checker(getResult);

                    });

                    //change the status for new orders to none.
                    var request = new RestRequest("bot_store/api/store", Method.POST);
                    request.RequestFormat = RestSharp.DataFormat.Json;
                    request.AddBody("none");
                    client.Execute(request);
                    SystemSounds.Exclamation.Play();
                }
            });
        }
        #endregion

        #region New Order Checker
        async void new_order_checker(string result)
        {
            await Task.Run(() =>
            {

                string getAction = "";
                string putAction = "";


                ///get request for new orders based on store id
                getAction = "bot_store/api/CurrentTransaction?request=pending&smscode=0000&storeid=" + result;
                putAction = "bot_store/api/CurrentTransaction/1?request=&sms=0000&storeid=" + result;

                var request = new RestRequest(getAction, Method.GET);
                var queryResult = client.Execute<List<order_data>>(request).Data;

                Application.Current.Dispatcher.Invoke((Action)delegate
                {

                    dynamic_construct(queryResult);
                });

                if (client.Execute(request).StatusCode == HttpStatusCode.OK)
                {
                    var updateRequest = new RestRequest(putAction, Method.PUT);
                    client.Execute(updateRequest);
                }
            });

        }
        #endregion

        #region Dynamic Construction of data
        void dynamic_construct(List<order_data> data)
        {
            try
            {
                DataGrid ordersGrid = new DataGrid();
                DataGrid info = new DataGrid();
                GroupBox group = new GroupBox();
                Grid grid = new Grid();
                Button btnConfirm = new Button();
                Button btnAddress = new Button();
                StackPanel stack = new StackPanel();
                StackPanel stack_horizontal = new StackPanel();
                StackPanel stack_horizontal1 = new StackPanel();
                StackPanel stack_horizontal2 = new StackPanel();
                StackPanel stack_horizontal3 = new StackPanel();
                StackPanel stack_horizontal4 = new StackPanel();
                StackPanel stack_horizontal5 = new StackPanel();

                Label name = new Label();
                Label name_data = new Label();
                Label location = new Label();
                Label or = new Label();
                Label or_num = new Label();
                Label c = new Label();
                Label cash = new Label();
                Label cont = new Label();
                Label contact = new Label();

                for (int i = 0; i < data.Count; i++)
                {
                    group = new GroupBox();
                    group.Name = data[i].or;
                    group.Header = "Duration: ";

                    stack_horizontal = new StackPanel();
                    stack_horizontal.Orientation = Orientation.Horizontal;
                    stack_horizontal.Margin = new Thickness(0, 0, 0, 0);
                    stack_horizontal1 = new StackPanel();
                    stack_horizontal1.Orientation = Orientation.Horizontal;
                    stack_horizontal1.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal2 = new StackPanel();
                    stack_horizontal2.Orientation = Orientation.Horizontal;
                    stack_horizontal2.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal3 = new StackPanel();
                    stack_horizontal3.Orientation = Orientation.Horizontal;
                    stack_horizontal3.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal4 = new StackPanel();
                    stack_horizontal4.Orientation = Orientation.Horizontal;
                    stack_horizontal4.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal5 = new StackPanel();
                    stack_horizontal5.Orientation = Orientation.Horizontal;
                    stack_horizontal5.Margin = new Thickness(0, 0, 0, 0);

                    stack = new StackPanel();
                    grid = new Grid();

                    or = new Label();
                    or.FontWeight = FontWeights.Bold;
                    or_num = new Label();
                    name = new Label();
                    name.FontWeight = FontWeights.Bold;
                    name_data = new Label();
                    cont = new Label();
                    cont.FontWeight = FontWeights.Bold;
                    contact = new Label();
                    location = new Label();
                    location.FontWeight = FontWeights.Bold;
                    c = new Label();
                    c.FontWeight = FontWeights.Bold;
                    cash = new Label();

                    btnAddress = new Button();
                    btnAddress.Style = address_button_style;

                    btnConfirm = new Button();
                    btnConfirm.Style = deliver_button_style;

                    or.Content = "Order no: ";
                    or_num.Content = data[i].or;
                    name.Content = "Name:     ";
                    name_data.Content = data[i].customer_name;
                    location.Content = "Address:   ";
                    btnAddress.Content = data[i].customer_location;
                    cont.Content = "Contact#:";
                    contact.Content = data[i].customer_contact;
                    c.Content = "Cash:       ";
                    cash.Content = data[i].customer_cash;

                    stack_horizontal.Children.Add(or);
                    stack_horizontal.Children.Add(or_num);
                    stack.Children.Add(stack_horizontal);
                    stack_horizontal2.Children.Add(name);
                    stack_horizontal2.Children.Add(name_data);
                    stack_horizontal3.Children.Add(cont);
                    stack_horizontal3.Children.Add(contact);
                    stack_horizontal4.Children.Add(c);
                    stack_horizontal4.Children.Add(cash);
                    stack.Children.Add(stack_horizontal2);
                    stack.Children.Add(stack_horizontal3);
                    stack.Children.Add(stack_horizontal4);

                    stack_horizontal1.Children.Add(location);
                    stack_horizontal1.Children.Add(btnAddress);
                    stack.Children.Add(stack_horizontal1);

                    ordersGrid = new DataGrid();
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Menu", typeof(string)));
                    dt.Columns.Add(new DataColumn("Type", typeof(string)));
                    dt.Columns.Add(new DataColumn("Size", typeof(string)));
                    dt.Columns.Add(new DataColumn("Qty", typeof(string)));
                    dt.Columns.Add(new DataColumn("Price", typeof(string)));

                    foreach (var o in data[i].orders)

                    {
                        dt.Rows.Add(o.order_name, o.order_type, o.order_size, o.order_quantity, o.order_price);
                    }

                    ordersGrid.ItemsSource = dt.DefaultView;

                    //confirm button
                    

                    if ((data[i].order_status == "pending") || (data[i].order_status == "confirming"))
                    {
                        btnConfirm.Tag = data[i].or;
                        btnConfirm.Content = "Confirm";
                        btnConfirm.Click += btnConfirm_Click;

                        //address button
                        btnAddress.Tag = data[i].or;
                        btnAddress.Click += btnAddress_Click;

                        //adding created data to observable collection.
                        dynamicConfirm.Add(new data_collection()
                        {
                            or = data[i].or,
                            customer_contact = data[i].customer_contact,
                            address = data[i].customer_location,
                            additional_info = data[i].additional_info,
                            store_longitude = data[i].store_longitude,
                            store_latitude = data[i].store_latitude,
                            longitude = data[i].customer_longitude,
                            latitude = data[i].customer_latitude,
                            date = data[i].date,
                            verification = data[i].verification,
                            group = group
                        });

                        stack.Children.Add(ordersGrid);
                        stack.Children.Add(btnConfirm);
                        group.Content = stack;
                        groupings.Add(group);
                    }
                }
            }
            catch (Exception)
            {
                //put error logs here...
            }

        }
        #endregion

        #region Button Confirm Click Event
        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            List<data_collection> list = dynamicConfirm.Where(x => x.or == btn.Tag.ToString()).ToList();
            string verification = "";
            string additional = "";
            string phone = "";
            //string storeid = "";
            foreach (var l in list)
            {
                verification = l.verification;
                additional = l.additional_info;
                phone = l.customer_contact;
            }

            string message = "Thank you for choosing Pizza Hut! Verification code is: " + verification;

            try
            {
                //MessageBox.Show(phone +" " + phone.Length.ToString());

                if (objclsSMS.sendMsg(this.port, phone, message))
                {
                   // MessageBox.Show("Message has sent successfully");
                    //this.statusBar1.Text = "Message has sent successfully";
                }
                else
                {
                  //  MessageBox.Show("Failed to send message");
                   // this.statusBar1.Text = "Failed to send message";
                }

            }
            catch (Exception)
            {
              // MessageBox.Show(ex.Message);
            }

            // MessageBox.Show(message);

            //MessageBox.Show(btn.Tag.ToString());
            string putAction = "bot_store/api/CurrentTransaction/3?request=" + btn.Tag.ToString() + "&sms=0000&storeid=0000";
            var updateRequest = new RestRequest(putAction, Method.PUT);
            client.Execute(updateRequest);


            var q = dynamicConfirm.IndexOf(dynamicConfirm.Where(X => X.or == btn.Tag.ToString()).FirstOrDefault());

            dynamicConfirm.RemoveAt(q);
            groupings.RemoveAt(q);

        }
        #endregion

        #region Button Address Click Event
        private void btnAddress_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            List<data_collection> list = dynamicConfirm.Where(x => x.or == btn.Tag.ToString()).ToList();
            string address = "";
            string additional_info = "";
            double longitude = 0;
            double latitude = 0;
            double store_longitude = 0;
            double store_latitude = 0;

            foreach (var l in list)

            {
                address = l.address;
                additional_info = l.additional_info;
                longitude = l.longitude;
                latitude = l.latitude;
                store_longitude = l.store_longitude;
                store_latitude = l.store_latitude;
            }

            //Messenger.Default.Send(new NotificationMessage("ShowMap"));

            MapsDetails maps = new MapsDetails();
            maps.custDetails(longitude, latitude, address, additional_info, store_longitude, store_latitude);
            maps.ShowDialog();

        }
        #endregion
        
        #region Get Pending Orders
        public void pending(string storeid)
        {
            try{

                dynamicConfirm.Clear();
                groupings.Clear();
                this.StoreID = storeid;
                var request = new RestRequest("bot_store/api/CurrentTransaction?request=all&smscode=0000&storeid=" + storeid, Method.GET);
                var queryResult = client.Execute<List<order_data>>(request).Data;

                Application.Current.Dispatcher.Invoke((Action)delegate
                {

                    dynamic_construct(queryResult);

                });

                tempoList = queryResult;

            }
            catch
            {

            }
          
        }
        #endregion

        private string _comport = string.Empty;
        public string Comport
        {
            get
            {
                return _comport;
            }
            set
            {
                _comport = value;
                RaisePropertyChanged("Comport");

            }
        }
    }
}
