﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using static GenericStore.Model.LoginDataItem;

namespace GenericStore.ViewModel
{
    public class StatusViewModel:ViewModelBase
    {
        DispatcherTimer pendingsTimer = new DispatcherTimer();
        public RelayCommand LogoutCommand { get; private set; }

        public StatusViewModel()
        {
            Messenger.Default.Register<List<user>>(this, "Message", (dataList) =>
            {
                foreach (var data in dataList)
                {
                    StoreID = data.store_id;
                    Name = data.firstname + " " + data.lastname;
                }
            });

            Messenger.Default.Register<string>(this, "Pendings", (pendings) =>
            {
                Pendings = pendings;
               
            });

            LogoutCommand = new RelayCommand(Logout);
        }

        private void p_timer()
        {
            pendingsTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            pendingsTimer.Interval = new TimeSpan(0, 0, 1);
            pendingsTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
           
        }

        private void Logout()
        {
            MessageBoxResult result = MessageBox.Show("Would you like to Logout?", "Logout", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Messenger.Default.Send(new NotificationMessage("LogoutWindow"));
            }         
        }

        private string _storeid = string.Empty;
        public string StoreID
        {
            get
            {
                return _storeid;
            }
            set
            {
                _storeid = value;
                RaisePropertyChanged("StoreID");
               
            }
        }

        private string _pendings = string.Empty;
        public string Pendings
        {
            get
            {
                return _pendings;
            }
            set
            {
                _pendings = value;
                RaisePropertyChanged("Pendings");
            }
        }

        private string _name = string.Empty;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }
    }
}
