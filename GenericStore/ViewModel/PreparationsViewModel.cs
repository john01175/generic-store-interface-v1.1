﻿using GalaSoft.MvvmLight.Messaging;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using static GenericStore.Model.ConfirmationDataItem;
using static GenericStore.Model.LoginDataItem;

namespace GenericStore.ViewModel
{
    public class PreparationsViewModel
    {
        public string StoreID;
        public string p;

        DispatcherTimer prepTimer = new DispatcherTimer();
        DispatcherTimer queryPrepTimer = new DispatcherTimer();
        DateTime currentdt;

        public ObservableCollection<data_collection> dynamicPreperations { get; set; } = new ObservableCollection<data_collection>();
        ObservableCollection<GroupBox> prepGroupings = new ObservableCollection<GroupBox>();
        List<order_data> tempoList;

        Style greenGroupbox = (Style)Application.Current.Resources["Green"];
        Style yellowGroupbox = (Style)Application.Current.Resources["Yellow"];
        Style redGroupbox = (Style)Application.Current.Resources["Red"];
        Style deliver_button_style = (Style)Application.Current.Resources["deliver_button_style"];
        Style address_button_style = (Style)Application.Current.Resources["address_button_style"];

        RestClient client = new RestClient("http://chatbotservices.somee.com");

        public PreparationsViewModel()
        {
            Messenger.Default.Register<List<user>>(this, "Message", (dataList) =>
            {
                foreach (var data in dataList)
                {

                   pending(data.store_id);
                

                }
            });


            d_timer();
            query_timer();
        }

        #region Preparation Duration Timer Initialization
        private void d_timer()
        {
            prepTimer.Tick += new EventHandler(prepTimer_Tick);
            prepTimer.Interval = new TimeSpan(0, 0, 1);
            prepTimer.Start();
        } 
        #endregion

        #region Preperations Timer
        private void prepTimer_Tick(object sender, EventArgs e)
        {
            //txtNofPreparation = dynamicPreperations.Count.ToString();
            this.currentdt = DateTime.Now;

            if (prepGroupings != null)
            {
                for (int i = 0; i < prepGroupings.Count; i++)
                {
                    TimeSpan span = this.currentdt - dynamicPreperations[i].date;
                    double difference = span.TotalSeconds;
                    TimeSpan sec = TimeSpan.FromSeconds(difference + 30);
                    double hours = 0;
                    double minutes = 0;

                    if (difference > 60)
                    {
                        hours = (difference / 60) / 60;
                        minutes = (((hours * 100) % 100) / 100) * 60;
                    }

                    else
                    {
                        minutes = difference / 60;

                    }

                    if ((minutes <= 5) && (hours < 1))
                    {
                        prepGroupings[i].Style = greenGroupbox;
                    }

                    else if ((minutes > 5) && (minutes < 10) && (hours < 1))
                    {
                        prepGroupings[i].Style = yellowGroupbox;
                    }

                    else
                    {
                        prepGroupings[i].Style = redGroupbox;
                    }

                    prepGroupings[i].Header = "Duration: " + hours.ToString("0") + "h: " + minutes.ToString("00") + "m: " + sec.Seconds.ToString("00") + "s";
                }
            }
        }
        #endregion

        #region Query Timer Initialization
        private void query_timer()
        {

            queryPrepTimer.Tick += new EventHandler(queryPrepTimer_Tick);
            queryPrepTimer.Interval = new TimeSpan(0, 0, 1);
            queryPrepTimer.Start();
        }
        #endregion

        #region Query Preperation Timer
        private async void queryPrepTimer_Tick(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                string pendings = dynamicPreperations.Count.ToString();
                Messenger.Default.Send(pendings, "Pendings");

                var getRequest = new RestRequest("bot_store/api/delivery", Method.GET);
                string getResult = client.Execute(getRequest).Content;
                getResult = getResult.Trim('"');


                if (getResult != "none")
                {

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action(() =>

                    new_order_checker(getResult)


                    ));
                    
                }
            });
        }
        #endregion


        #region New Order Checker
        async void new_order_checker(string result)
        {
            await Task.Run(() =>
            {

                string getAction = "";
                string putAction = "";


                //MessageBox.Show(result);
                getAction = "bot_store/api/CurrentTransaction?request=unread_prep&smscode=" + result + "&storeid="+StoreID;
                putAction = "bot_store/api/CurrentTransaction/2?request=&sms="+result+"&storeid="+StoreID;


                var request = new RestRequest(getAction, Method.GET);
                var queryResult = client.Execute<List<order_data>>(request).Data;

                Application.Current.Dispatcher.Invoke((Action)delegate
                {

                    dynamic_construct(queryResult);
                });

                if (client.Execute(request).StatusCode == HttpStatusCode.OK)
                {
                    var updateRequest = new RestRequest(putAction, Method.PUT);
                    client.Execute(updateRequest);

                    var request1 = new RestRequest("bot_store/api/delivery", Method.POST);
                    request1.RequestFormat = RestSharp.DataFormat.Json;
                    request1.AddBody("none");
                    client.Execute(request1);
                    
                }


            });

        } 
        #endregion

        void dynamic_construct(List<order_data> data)
        {
            try
            {
                DataGrid ordersGrid = new DataGrid();
                DataGrid info = new DataGrid();
                GroupBox group = new GroupBox();
                Grid grid = new Grid();
                Button btnConfirm = new Button();
                Button btnAddress = new Button();
                StackPanel stack = new StackPanel();
                StackPanel stack_horizontal = new StackPanel();
                StackPanel stack_horizontal1 = new StackPanel();
                StackPanel stack_horizontal2 = new StackPanel();
                StackPanel stack_horizontal3 = new StackPanel();
                StackPanel stack_horizontal4 = new StackPanel();
                StackPanel stack_horizontal5 = new StackPanel();

                Label name = new Label();
                Label name_data = new Label();
                Label location = new Label();
                Label or = new Label();
                Label or_num = new Label();
                Label c = new Label();
                Label cash = new Label();
                Label cont = new Label();
                Label contact = new Label();

                for (int i = 0; i < data.Count; i++)
                {
                    group = new GroupBox();
                    group.Name = data[i].or;
                    group.Header = "Duration: ";

                    stack_horizontal = new StackPanel();
                    stack_horizontal.Orientation = Orientation.Horizontal;
                    stack_horizontal.Margin = new Thickness(0, 0, 0, 0);
                    stack_horizontal1 = new StackPanel();
                    stack_horizontal1.Orientation = Orientation.Horizontal;
                    stack_horizontal1.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal2 = new StackPanel();
                    stack_horizontal2.Orientation = Orientation.Horizontal;
                    stack_horizontal2.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal3 = new StackPanel();
                    stack_horizontal3.Orientation = Orientation.Horizontal;
                    stack_horizontal3.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal4 = new StackPanel();
                    stack_horizontal4.Orientation = Orientation.Horizontal;
                    stack_horizontal4.Margin = new Thickness(0, 0, 0, 0);

                    stack_horizontal5 = new StackPanel();
                    stack_horizontal5.Orientation = Orientation.Horizontal;
                    stack_horizontal5.Margin = new Thickness(0, 0, 0, 0);

                    stack = new StackPanel();
                    grid = new Grid();

                    or = new Label();
                    or.FontWeight = FontWeights.Bold;
                    or_num = new Label();
                    name = new Label();
                    name.FontWeight = FontWeights.Bold;
                    name_data = new Label();
                    cont = new Label();
                    cont.FontWeight = FontWeights.Bold;
                    contact = new Label();
                    location = new Label();
                    location.FontWeight = FontWeights.Bold;
                    c = new Label();
                    c.FontWeight = FontWeights.Bold;
                    cash = new Label();

                    btnAddress = new Button();
                    btnAddress.Style = address_button_style;

                    btnConfirm = new Button();
                    btnConfirm.Style = deliver_button_style;

                    or.Content = "Order no: ";
                    or_num.Content = data[i].or;
                    name.Content = "Name:     ";
                    name_data.Content = data[i].customer_name;
                    location.Content = "Address:   ";
                    btnAddress.Content = data[i].customer_location;
                    cont.Content = "Contact#:";
                    contact.Content = data[i].customer_contact;
                    c.Content = "Cash:       ";
                    cash.Content = data[i].customer_cash;

                    stack_horizontal.Children.Add(or);
                    stack_horizontal.Children.Add(or_num);
                    stack.Children.Add(stack_horizontal);
                    stack_horizontal2.Children.Add(name);
                    stack_horizontal2.Children.Add(name_data);
                    stack_horizontal3.Children.Add(cont);
                    stack_horizontal3.Children.Add(contact);
                    stack_horizontal4.Children.Add(c);
                    stack_horizontal4.Children.Add(cash);
                    stack.Children.Add(stack_horizontal2);
                    stack.Children.Add(stack_horizontal3);
                    stack.Children.Add(stack_horizontal4);

                    stack_horizontal1.Children.Add(location);
                    stack_horizontal1.Children.Add(btnAddress);
                    stack.Children.Add(stack_horizontal1);

                    ordersGrid = new DataGrid();
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Menu", typeof(string)));
                    dt.Columns.Add(new DataColumn("Type", typeof(string)));
                    dt.Columns.Add(new DataColumn("Size", typeof(string)));
                    dt.Columns.Add(new DataColumn("Qty", typeof(string)));
                    dt.Columns.Add(new DataColumn("Price", typeof(string)));

                    foreach (var o in data[i].orders)

                    {
                        dt.Rows.Add(o.order_name, o.order_type, o.order_size, o.order_quantity, o.order_price);
                    }

                    ordersGrid.ItemsSource = dt.DefaultView;

                    btnConfirm.Tag = data[i].or;

                    if ((data[i].order_status == "preparing") || (data[i].order_status == "unread_prep"))
                    {
                        btnConfirm.Content = "Deliver";
                        btnConfirm.Click += btnDeliver_Click;

                        btnAddress.Tag = data[i].or;
                        btnAddress.Click += btnAddressForPreparations_Click;

                        dynamicPreperations.Add(new data_collection()
                        {
                            or = data[i].or,
                            address = data[i].customer_location,
                            additional_info = data[i].additional_info,
                            store_longitude = data[i].store_longitude,
                            store_latitude = data[i].store_latitude,
                            longitude = data[i].customer_longitude,
                            latitude = data[i].customer_latitude,
                            date = data[i].date,
                            verification = data[i].verification,
                            group = group
                        });

                        stack.Children.Add(ordersGrid);
                        stack.Children.Add(btnConfirm);
                        group.Content = stack;
                        prepGroupings.Add(group);
                    }
                }
            }
            catch
            {
                //error logs here
            }

        }

        private void btnAddressForPreparations_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            List<data_collection> list = dynamicPreperations.Where(x => x.or == btn.Tag.ToString()).ToList();
            string address = "";
            string additional_info = "";
            double longitude = 0;
            double latitude = 0;
            double store_longitude = 0;
            double store_latitude = 0;

            foreach (var l in list)

            {
                address = l.address;
                additional_info = l.additional_info;
                longitude = l.longitude;
                latitude = l.latitude;
                store_longitude = l.store_longitude;
                store_latitude = l.store_latitude;
            }

            MapsDetails maps = new MapsDetails();
            maps.custDetails(longitude, latitude, address, additional_info, store_longitude, store_latitude);
            maps.ShowDialog();

        }

        private void btnDeliver_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            List<order_data> list = tempoList.Where(x => x.or == btn.Tag.ToString()).ToList();

            string phone = "";
            //string storeid = "";
            foreach (var l in list)
            {
                phone = l.customer_contact;
            }

            var q = dynamicPreperations.IndexOf(dynamicPreperations.Where(X => X.or == btn.Tag.ToString()).FirstOrDefault());

            dynamicPreperations.RemoveAt(q);
            prepGroupings.RemoveAt(q);

            string message = "Printing receipt";
            MessageBox.Show(message);
            //dynamicPreperations.RemoveAt(0);
            //prepGroupings.RemoveAt(0);
        }

        public void pending(string storeid)
        {
            try
            {
                dynamicPreperations.Clear();
                prepGroupings.Clear();
                //tempoList.Clear();

                this.StoreID = storeid;
                var request = new RestRequest("bot_store/api/CurrentTransaction?request=all&smscode=0000&storeid=" + storeid, Method.GET);
                var queryResult = client.Execute<List<order_data>>(request).Data;

                Application.Current.Dispatcher.Invoke((Action)delegate
                {

                    dynamic_construct(queryResult);

                });

                tempoList = queryResult;
            }
            catch { //error logs here..
                }
           

        }
    }
}
